# VarvaraPSP

An implementation of the [Uxn virtual machine](https://wiki.xxiivv.com/site/uxn.html) and [Varvara devices](https://wiki.xxiivv.com/site/varvara.html) for the Sony PlayStation Portable.

This implementation began as a port of the uxnemu SDL implementation, but the SDL components were stripped out in favour of native API calls, which lightened the dependenies and in some cases improved performance. The current performance is fantastic and can run even the most demanding software I'm aware of, such as [Oquonie](https://100r.co/site/oquonie.html) and [Fourtette](https://github.com/nf/fourtette)! Simpler programs such as [Donsol](https://100r.co/site/donsol.html) and [oneko-uxn](https://github.com/hikari-no-yume/oneko-uxn) also run great, of course.

# Running

Create a `VarvaraPSP` directory in the `/PSP/GAME` directory of your memory stick (`/PSP/GAME/VarvaraPSP`), and copy the EBOOT.PBP file there, along with your Uxn ROM files. The `launcher.rom` file will be loaded when VarvaraPSP is started. If this file cannot be loaded an error will be shown.

Larger ROMs such as [Oquonie](https://100r.co/site/oquonie.html) can only currently be run by renaming them to `launcher.rom` and running VarvaraPSP.

# Controls

- Circle is A (ctrl on PC)
- Cross is B (alt on PC)
- Start/Select are as one might expect
- The analog slider moves the mouse cursor
- Right trigger is left click (mouse1)
- Left trigger is right click (mouse3)
- Triangle restarts the emulator, reloading `launcher.rom`

# Building

After installing the PSPDEV toolchain, run the following commands to build VarvaraPSP:

```
mkdir build && cd build
psp-cmake ..
make
```

Note: If you get errors related to u64/etc types not being defined, you'll need to ensure you're using pspsdk 2f723c3 (2023-10-03) or later. Alternatively, add `#include <psptypes.h>` near the top of your src/rtc/psprtc.h file if updating the entire SDK is impractical.

# Missing Devices

- Keyboard support
- Console device (stdin/stdout)

# Additional Art

Many thanks to [Rek](https://kokorobot.ca/site/home.html) for the Uxn wallpaper art, used under BY-NC-SA4.0 license.