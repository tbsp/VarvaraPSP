#include <psprtc.h>

#include "../uxn.h"
#include "datetime.h"

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

int day_of_year(int year, int month, int day)
{
	int d = day;
	for (int m = 1; m < month; m++) {
		d += sceRtcGetDaysInMonth(year, month);
	}
	return d;
}

Uint8
datetime_dei(Uxn *u, Uint8 addr)
{
	pspTime t;
	sceRtcGetCurrentClockLocalTime(&t);
	int yday = (addr == 0xc8 || addr == 0xc9) ? day_of_year(t.year, t.month, t.day) : 0;
	switch(addr) {
		case 0xc0: return t.year >> 8;
		case 0xc1: return t.year;
		case 0xc2: return t.month - 1; // Months start from 1 in pspTime.
		case 0xc3: return t.day;
		case 0xc4: return t.hour;
		case 0xc5: return t.minutes;
		case 0xc6: return t.seconds;
		case 0xc7: return sceRtcGetDayOfWeek(t.year, t.month, t.day);
		case 0xc8: return yday >> 8;
		case 0xc9: return yday;
		case 0xca: return -1; // No Daylight Saving Time information available.
		default: return u->dev[addr];
	}
}
